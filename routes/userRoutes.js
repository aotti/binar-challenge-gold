"use strict"
const express = require('express');
// const app = express();
const router = express.Router();
const { userMiddle } = require('../middlewares/userMiddleware');
const { nullCheck, passwordLength } = require('../functions');
const getUserController = require('../controllers/userController');
const userController = new getUserController();

// MIDDLEWARE
router.use(userMiddle);

// ROUTE
router
    .get('/show/:key', (req, res) => {
        userController.getAllUser().then(result => {
            res.status(200).json({
                status: 200,
                message: 'berhasil dapat semua data',
                data: result
            })
        })
        .catch(err => { res.status(500).send(err) })
    })
    // REGISTER
    .post('/register', (req, res) => {
        const arrayForCheck = [req.body.username, req.body.password, req.body.email];
        if(nullCheck(arrayForCheck))
            res.send('data tidak boleh kosong!')
        else {
            if(passwordLength(req.body.password)) {
                userController.hashPassword(req.body.password).then(hash => {
                    userController.registerUser(req.body, hash).then(result => {
                        res.status(200).json({
                            status: 200,
                            message: 'anda berhasil register',
                            data: result
                        })
                    })
                    .catch(err => { res.status(500).send(err) })
                })
                .catch(err => { res.status(500).send(err) })
            }
            else 
                res.send('password atleast must have 6 letters!')
        }
    })
    // LOGIN
    .post('/login', (req, res) => {
        const arrayForCheck = [req.body.username, req.body.password];
        if(nullCheck(arrayForCheck))
            res.send('data tidak boleh kosong!')
        else {
            userController.loginAuth(req.body.username, req.body.password).then(result => {
                res.status(200).json({
                    status: 200,
                    message: `selamat datang, ${result?.username}`,
                    data: result
                })
            })
            .catch(err => { res.status(500).send(err) })
        }
    })
    .delete('/delete/:id', (req, res) => {
        userController.deleteUser(req.params.id).then(result => {
            if(nullCheck([result]))
                res.status(200).send('id tidak ada!')
            else {
                res.status(200).json({
                    status: 200,
                    message: `data user terhapus`,
                    data: result
                })
            }
        })
        .catch(err => { res.status(500).send(err) })
    });

module.exports = router