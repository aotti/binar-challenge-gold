"use strict"
const express = require('express');
// const app = express();
const router = express.Router();
const getOrderController = require('../controllers/orderController');
const { nullCheck } = require('../functions');
const orderController = new getOrderController();

router.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

// ROUTE
router
    .get('/list', (req, res) => {
        orderController.getAllOrder().then(result => {
            res.status(200).json({
                status: 200,
                message: 'berhasil dapat semua order',
                data: result
            })
        })
        .catch(err => { res.status(500).send(err) })
    })
    .get('/total', (req, res) => {
        orderController.getAllOrderTotal().then(result => {
            res.status(200).json({
                status: 200,
                message: 'berhasil dapat semua order v2',
                data: result
            })
        })
        .catch(err => { res.status(500).send(err) })
    })
    .get('/details', (req, res) => {
        orderController.getOrderDetails().then(result => {
            res.status(200).json({
                status: 200,
                message: 'berhasil dapat detail tiap order',
                data: result
            })
        })
        .catch(err => { res.status(500).send(err) })
    })
    // INSERT INTO
    .post('/create', (req, res) => {
        const arrayForCheck = [req.body.UserId, req.body.ItemId, req.body.status, req.body.jumlahBarang, req.body.subTotal];
        if(nullCheck(arrayForCheck))
            res.send('data tidak boleh kosong!')
        else {
            orderController.createOrder(req.body).then(result => {
                res.status(200).json({
                    status: 200,
                    message: 'berhasil membuat order',
                    data: result
                })
            })
            .catch(err => { res.status(500).send(err) })
        }
    })
    // UPDATE DETAIL
    .put('/update/:id', (req, res) => {
        const arrayForCheck = [req.params.id, req.body.status];
        if(nullCheck(arrayForCheck))
            res.send('data tidak boleh kosong!')
        else {
            orderController.updateOrderStatus(req.params.id, req.body).then(result => {
                res.status(200).json({
                    status: 200,
                    message: 'berhasil update status order',
                    data: result
                })
            })
            .catch(err => { res.status(500).send(err) })
        }
    });

module.exports = router