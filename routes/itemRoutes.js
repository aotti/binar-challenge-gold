"use strict"
const express = require('express');
// const app = express();
const router = express.Router();
const getItemController = require('../controllers/itemController');
const itemController = new getItemController();

router.use((req, res, next) => {
    console.log(`${req.method} ${req.url}`);
    next();
});

// ROUTE
router
    .get('/list', (req, res) => {
        itemController.getAllItem().then(result => {
            res.status(200).json({
                status: 200,
                message: 'berhasil dapat semua item',
                data: result
            })
        })
        .catch(err => { res.status(500).send(err) })
    });

// app.use('/', router)
module.exports = router