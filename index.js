const express = require('express');
const app = express();
const swaggerUI = require('swagger-ui-express');
const swaggerDoc = require('./swagger.json');
const port = 3e3;
const userRoutes = require('./routes/userRoutes');
const itemRoutes = require('./routes/itemRoutes');
const orderRoutes = require('./routes/orderRoutes');

app.use('/apidocs', swaggerUI.serve, swaggerUI.setup(swaggerDoc));

// UNTUK METHOD GET AGAR DATA YG TAMPIL JADI JSON
app.use(express.json());
// UNTUK METHOD POST AGAR BISA AMBIL VALUE DARI INPUT FORM
app.use(express.urlencoded({ extended: true }));

app.get('/home', (req, res) => {
    res.send('Selamat Datang, Anda berhasil deploy!')
})
app.use('/user', userRoutes);
app.use('/item', itemRoutes);
app.use('/order', orderRoutes);

app.get('/', (req, res) => {
    res.send('Selamat Datang di Meikarta')
})

app.listen(port, () => console.log(`listening to port ${port}`));