function nullCheck(array) {
    // KONDISI !v = NULL, UNDEFINED, NAN, 0, FALSE
    return array.some(v => !v)
}

function passwordLength(pass) {
    return pass.length < 6 ? false : true;
}

module.exports = {
    nullCheck,
    passwordLength
}