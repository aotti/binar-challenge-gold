'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Items', [{
      namaItem: 'Oreo 8gr',
      jenisItem: 'makanan',
      hargaItem: 2000,
      kadaluarsa: new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate())
    }, {
      namaItem: 'Ultra Milk 250ml',
      jenisItem: 'minuman',
      hargaItem: 5000,
      kadaluarsa: new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate())
    }, {
      namaItem: 'Chitato 45gr',
      jenisItem: 'makanan',
      hargaItem: 13000,
      kadaluarsa: new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate())
    }, {
      namaItem: 'Hydro Coco 300ml',
      jenisItem: 'minuman',
      hargaItem: 10000,
      kadaluarsa: new Date(new Date().getFullYear(), new Date().getMonth() + 1, new Date().getDate())
    }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete('Items', null, {});
  }
};
