'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Order);
    }
  }
  User.init({
    username: {
      type: DataTypes.STRING,
      validate: {
        len: {
          args: [4, 12],
          msg: 'username must be 4 - 12 letters long!'
        }
      }
    },
    password: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {msg: 'Wrong email format!'}
      }
    }
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};