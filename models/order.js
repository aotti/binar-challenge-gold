'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Order.User = this.belongsTo(models.User);
      Order.Item = this.belongsTo(models.Item);
    }
  }
  Order.init({
    UserId: DataTypes.INTEGER,
    ItemId: DataTypes.INTEGER,
    status: DataTypes.STRING,
    jumlahBarang: DataTypes.INTEGER,
    subTotal: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};