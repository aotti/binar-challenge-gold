const { Item } = require('../models/model'); 

class ItemController {
    getAllItem() {
        return Item.findAll({
            // PILIH KOLOM YG AKAN DITAMPILKAN
            attributes: ['id', 'namaItem', 'jenisItem', 'hargaItem', 'kadaluarsa']
        })
    }
}

module.exports = ItemController