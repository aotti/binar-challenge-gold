const { User } = require('../models/model'); 
const bcrypt = require('bcrypt');
const saltRounds = 10;

class UserController {
    hashPassword(password) {
        return bcrypt.hash(password, saltRounds)
    }

    #comparePassword(password, hashedPassword) {
        return bcrypt.compare(password, hashedPassword)
    }

    getAllUser() {
        return User.findAll({
            // PILIH KOLOM YG AKAN DITAMPILKAN
            attributes: ['id', 'username', 'password', 'email']
        })
    }

    registerUser(reqb, hashedPassword) {
        return User.create({
            username: reqb.username,
            password: hashedPassword,
            email: reqb.email
        })
    }
    
    loginAuth(username, password) {
        const processAuth = new Promise((resolve, reject) => {
            this.#loginAuthOne(username).then(getPassword => {
                this.#comparePassword(password, getPassword.password).then(decrypted => {
                    if(decrypted) {
                        resolve(this.#loginAuthTwo(username, getPassword.password))
                    }
                    else {
                        resolve('username / password salah!')
                    }
                })
                .catch(err => { reject(err) })
            })
            .catch(err => { reject(err) })
        })
        return processAuth
    }

    #loginAuthOne(username) {
        return User.findOne({
            attributes: ['id', 'username', 'password'],
            where: {
                username: username
            }
        })
    }

    #loginAuthTwo(username, password) {
        return User.findOne({
            attributes: ['id', 'username', 'password', 'email'],
            where: {
                username: username,
                password: password
            }
        })
    }

    updateUser(id, reqb) {
        const arrayForCheck = [reqb.username, reqb.password, reqb.email];
        if (nullCheck(arrayForCheck)) {
            return 'data tidak boleh kosong!'
        } 
        else {
            return User.update({
                username: reqb.username,
                password: reqb.password,
                email: reqb.email
            }, {
                where: {id: id}
            })
        }
    }
    
    deleteUser(id) {
        return User.destroy({
            where: {id: id}
        })
    }
}

module.exports = UserController