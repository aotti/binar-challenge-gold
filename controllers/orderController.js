const { Order, sequelize } = require('../models/model'); 

class OrderController {
    getAllOrder() {
        return Order.findAll({
            // PILIH KOLOM YG AKAN DITAMPILKAN
            attributes: ['id', 'UserId', 'ItemId', 'status', 'jumlahBarang', 'subTotal']
        })
    }
    
    getAllOrderTotal() {
        return Order.findAll({
            // PILIH KOLOM YG AKAN DITAMPILKAN
            attributes: ['UserId', [sequelize.fn('sum', sequelize.col('subTotal')), 'total']],
            group: 'UserId',
            order: [['UserId', 'ASC']]
        })
    }
    
    getOrderDetails() {
        return Order.findAll({
            attributes: ['id', 'UserId', 'ItemId', 'status', 'jumlahBarang', 'subTotal'],
            include: [{
                association: Order.User,
                attributes: ['username']
            }, {
                association: Order.Item,
                attributes: ['namaItem', 'hargaItem']
            }]
        })
    }

    createOrder(reqb) {
        return Order.create({
            UserId: reqb.UserId,
            ItemId: reqb.ItemId,
            status: reqb.status,
            jumlahBarang: reqb.jumlahBarang,
            subTotal: reqb.subTotal
        })
    }

    updateOrderStatus(id, reqb) {
        return Order.update({
            status: reqb.status
        }, {
            where: {id: id}
        })
    }
}

module.exports = OrderController